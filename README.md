# VMailSysConf
Django project to manage virtual mail domains of Postfix.

## Installation

### Development

- Generate secret key in settings.py
- Configure database settings in settings.py
- Migrate database
```
python manage.py migrate
```
- Create superuser
```
python manage.py createsuperuser
```
- Add sites DNS domain to settings.py
- Run development server
```
python manage.py runserver 0.0.0.0:8001
```

### Development

The use of the build-in development server is discouraged for productional use.

### ToDo

- [x] Show Domain Admin status in DomainDetailView
- [ ] Reusable validation handling for Mailbox/Alias forms/views
- [ ] Change table names without appname
- [ ] Readd SpamAlias Handling
    - User can only create random SpamAlias
    - User can change validity of SpamAlias
    - User can delete SpamAlias but not recreate
    - Domain Admin can recreate specific SpamAlias for mailbox
- [ ] Remake DomainCreateView
    - add first Domain Admin mailbox when creating
    - prompt for rfc2142 Aliases
