"""vmailsysconf URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, re_path

from django.conf.urls import include
from django.conf import settings
from django.apps import apps

from core.views.dashboard import DashboardView
from core.views.csc import CscView
from core.views.auth_views import change_password
from core.views.auth_views import login_success_dispatcher

urlpatterns = [
    # core app
    re_path(r'^$',          DashboardView.as_view(),    name="dashboard_view"),
    re_path(r'^csc/$',       CscView.as_view(),          name="csc_view"),
    re_path(r'^domain/',    include('core.urls')),

    path('admin/', admin.site.urls),

    # User login|logout
    path('accounts/', include('django.contrib.auth.urls')),
    path('login_success', login_success_dispatcher, name="login_success_view"),

    # Password Change for users
    re_path(r'^password/$', change_password, name='change_password'),
]

if settings.DEBUG:
    if apps.is_installed("debug_toolbar"):
        import debug_toolbar
        urlpatterns = [
            re_path(r'^__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns
