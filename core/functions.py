import crypt
import random
import string

class SHA512Crypt():
    """
    Creates salted SHA512 crypt hash like it's used by 
        doveadm -v pw -p "pw" -s SHA512-CRYPT
    """
    def __get_salt__(self, char_count):
        return ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(255))[:char_count]

    def get_crypt(self, pw):
        salt = self.__get_salt__(16)
        salt = "%s%s" % ("$6$", salt)
        
        cryptstring = crypt.crypt(pw, salt)
        return cryptstring

