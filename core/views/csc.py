# global Python imports
import logging

# Django specific imports
from django.shortcuts import render
from django.shortcuts import redirect
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from braces.views import LoginRequiredMixin
from django.db.models import Sum, Count

# App specific imports
from core.models import Domain
from core.models import Alias
from core.models import Mailbox
from core.forms import DomainEditForm

# Get an instance of the logger
logger = logging.getLogger(__name__)

class CscView(LoginRequiredMixin, TemplateView):
    template_name = "csc.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        mailbox = None
        try:
            mailbox = Mailbox.objects.get(user=self.request.user)
        except Exception:
            pass
        context['mailbox'] = mailbox

        alias = None
        try:
            aliases = Alias.objects.filter(destination__contains=self.request.user.email).values_list('source', flat=True)
        except Exception:
            pass
        context['aliases'] = aliases

        return context
