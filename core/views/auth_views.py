from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required

from core.models import User
from core.models import Mailbox

# https://simpleisbetterthancomplex.com/tips/2016/08/04/django-tip-9-password-change-form.html
@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()

            # update mailbox password
            mailbox = Mailbox.objects.get(user=user)
            if mailbox:
                mailbox.password = form.cleaned_data['new_password1']
                mailbox.save()

            update_session_auth_hash(request, user)  # Important!

            messages.success(request, 'Your password was updated successfully!')
            return redirect('change_password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'accounts/change_password.html', {
        'form': form
    })

@login_required
def login_success_dispatcher(request):
    """
    Redirects users based on whether they are in the admins group
        https://stackoverflow.com/a/16824337/7523861
    """
    if request.user.is_superuser or request.user.groups.filter(name__startswith="DA_"):
        # user is super user or a domain admin
        return redirect(reverse('dashboard_view'))
    else:
        return redirect(reverse("csc_view"))
