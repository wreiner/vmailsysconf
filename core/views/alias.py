# global Python imports
import logging
import re

# Django specific imports
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from braces.views import LoginRequiredMixin
from django.contrib.auth.models import Group
from django.contrib import messages
from django.core.validators import validate_email
from django.core.exceptions import ValidationError

# App specific imports
from core.models import Mailbox
from core.models import User
from core.models import Domain
from core.models import Alias
from core.forms import AliasEditForm

# Get an instance of the logger
logger = logging.getLogger(__name__)

class AliasCreateView(LoginRequiredMixin, CreateView):
    model = Alias
    form_class = AliasEditForm
    template_name = "alias_form.html"
    login_url = "/login/"

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            return HttpResponseRedirect(self.success_url)
        else:
            return super(AliasCreateView, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AliasCreateView, self).get_context_data(**kwargs)

        context['title'] = "Alias | Add a alias"
        context['nav_section'] = "alias"
        context['nav_sub_section'] = "alias_create"
        return context

    def get_success_url(self):
        domain = self.object.domain
        return reverse('core:domain-detail-view', kwargs={'pk': domain.id})

    def form_valid(self, form):
        # read domain from url parameter
        domain = self.kwargs['domain']
        form.instance.domain = get_object_or_404(Domain, pk=self.kwargs['domain'])

        repattern = "@{}$".format(form.instance.domain)
        error_found = False

        source = form.cleaned_data["source"]
        try:
            validate_email(source)
        except ValidationError:
            form.add_error('source',
                 "Address {} is not a valid email address." \
                 .format(source))
            return super(AliasCreateView, self).form_invalid(form)

        # FIXME test email sanity check
        if not re.search(repattern, source):
            form.add_error('source',
                "You are trying to add an alias for the domain {}. " \
                 "You are not using this domain ({}) for the source address {}" \
                 .format(form.instance.domain, form.instance.domain, source))
            error_found = True

        srcparts = source.split('@')
        if not srcparts:
            form.add_error('source',
                'The source address {} is not an email address.'.format(source))
            return super(AliasCreateView, self).form_invalid(form)

        if '-' in srcparts[0]:
                form.add_error("source",
                    "The source address {} contains a hyphen character. " \
                    "This mailsystem uses the hyphen character in the user part " \
                    "of an email address as the tag for a mailbox.".format(source))
                return super(AliasCreateView, self).form_invalid(form)

        if Mailbox.objects.filter(username=srcparts[0], domain__domain=srcparts[1]).exists():
                form.add_error('source',
                    'The source address {} is already an existing mailbox.'.format(source))
                return super(AliasCreateView, self).form_invalid(form)

        destination = form.cleaned_data["destination"]
        dests = destination.split(",")
        for dest in dests:
            if not re.search(repattern, dest):
                form.add_error('destination',
                    'You are trying to add an alias for the domain {}. You are not using domain {} for the destination address {}'.format(form.instance.domain, form.instance.domain, dest))
                error_found = True

            if source == dest:
                form.add_error('destination',
                    'The destination address {} is the same as the source address.'.format(dest))
                error_found = True

            try:
                validate_email(dest)
            except ValidationError:
                form.add_error('destination',
                     "Address {} is not a valid email address." \
                     .format(dest))
                return super(AliasCreateView, self).form_invalid(form)

            destparts = dest.split('@')
            if not destparts:
                form.add_error('destination',
                    'The destination address {} is not an email address.'.format(dest))
                return super(AliasCreateView, self).form_invalid(form)

            # check if destination is mailbox or alias
            if (not Alias.objects.filter(source=dest).exists() and
                    not Mailbox.objects.filter(username=destparts[0], domain__domain=destparts[1]).exists()):
                print("no alias or mailbox found")
                form.add_error('destination',
                    'The destination address {} is neither an existing mailbox nor an exisiting alias.'.format(dest))
                return super(AliasCreateView, self).form_invalid(form)

        if error_found:
            return super(AliasCreateView, self).form_invalid(form)

        logger.debug('[{}] will create alias [{} -> {}] for domain {}'.format(self.request.user, source, destination, form.instance.domain))

        return super(AliasCreateView, self).form_valid(form)

class AliasEditView(LoginRequiredMixin, UpdateView):
    model = Alias
    # http://stackoverflow.com/q/39020226/7523861
    form_class = AliasEditForm
    template_name = "alias_form.html"

    login_url = "/login/"

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            return HttpResponseRedirect(self.success_url)
        else:
            return super(AliasEditView, self).post(request, *args, **kwargs)

    def get_success_url(self):
        domain = self.object.domain
        return reverse('core:domain-detail-view', kwargs={'pk': domain.id})

    def get_context_data(self, **kwargs):
        context = super(AliasEditView, self).get_context_data(**kwargs)

        context['update_or_create'] = "update"
        return context

    def form_valid(self, form):
        # read domain from url parameter
        domain = self.kwargs['domain']
        form.instance.domain = get_object_or_404(Domain, pk=self.kwargs['domain'])

        repattern = "@{}$".format(form.instance.domain)
        error_found = False

        source = form.cleaned_data["source"]
        try:
            validate_email(source)
        except ValidationError:
            form.add_error('source',
                 "Address {} is not a valid email address." \
                 .format(source))
            return super(AliasEditView, self).form_invalid(form)

        # FIXME test email sanity check
        if not re.search(repattern, source):
            form.add_error('source',
                "You are trying to add an alias for the domain {}. " \
                 "You are not using this domain ({}) for the source address {}" \
                 .format(form.instance.domain, form.instance.domain, source))
            error_found = True

        destination = form.cleaned_data["destination"]
        dests = destination.split(",")
        for dest in dests:
            if not re.search(repattern, dest):
                form.add_error('destination',
                    'You are trying to add an alias for the domain {}. You are not using domain {} for the destination address {}'.format(form.instance.domain, form.instance.domain, dest))
                error_found = True

            if source == dest:
                form.add_error('destination',
                    'The destination address {} is the same as the source address.'.format(dest))
                error_found = True

            try:
                validate_email(dest)
            except ValidationError:
                form.add_error('destination',
                     "Address {} is not a valid email address." \
                     .format(dest))
                return super(AliasEditView, self).form_invalid(form)

            # FIXME test email sanity check
            destparts = dest.split('@')
            if not destparts:
                form.add_error('destination',
                    'The destination address {} is not an email address.'.format(dest))
                return super(AliasEditView, self).form_invalid(form)

            # check if destination is mailbox or alias
            if (not Alias.objects.filter(source=dest).exists() and
                    not Mailbox.objects.filter(username=destparts[0], domain__domain=destparts[1]).exists()):
                print("no alias or mailbox found")
                form.add_error('destination',
                    'The destination address {} is neither an existing mailbox nor an exisiting alias.'.format(dest))
                return super(AliasEditView, self).form_invalid(form)

        if error_found:
            return super(AliasEditView, self).form_invalid(form)

        logger.debug('[{}] will create alias [{} -> {}] for domain {}'.format(self.request.user, source, destination, form.instance.domain))

        return super(AliasEditView, self).form_valid(form)

class AliasDeleteView(LoginRequiredMixin, DeleteView):
    model = Alias
    template_name = "alias_confirm_delete.html"

    login_url = "/login/"

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            return HttpResponseRedirect(self.get_success_url(self))
        else:
            return super(AliasDeleteView, self).post(request, *args, **kwargs)

    def get_success_url(self):
        domain = self.object.domain
        return reverse('core:domain-detail-view', kwargs={'pk': domain.id})

    def delete(self, request, *args, **kwargs):
        #object = self.get_object()

        #logger.info("[{}] will delete alias {}@{}".format(request.user, object.username, object.domain))
        response = super(AliasDeleteView, self).delete(request, *args, **kwargs)

        #logger.info("[{}] will delete Django user {}@{}".format(request.user, object.username, object.domain))
        #User.objects.get(email = '{}@{}'.format(object.username, object.domain)).delete()

        return response
