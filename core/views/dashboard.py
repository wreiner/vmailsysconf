# global Python imports
import logging

# Django specific imports
from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from braces.views import LoginRequiredMixin
from django.db.models import Sum, Count

# App specific imports
from core.models import Domain
from core.models import Mailbox
from core.forms import DomainEditForm

# Get an instance of the logger
logger = logging.getLogger(__name__)

class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser and not request.user.groups.filter(name__startswith="DA_"):
            return redirect(reverse('csc_view'))

        return super(DashboardView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        domains = ""
        # get domains user is domain admin of
        if self.request.user.is_superuser:
            domains = Domain.objects.all()
        else:
            print("getting da groups for user {}".format(self.request.user))
            da_groups = self.request.user.groups.filter(name__startswith="DA_")
            if not da_groups:
                return redirect()

            for group in da_groups:
                print("user is in group {}".format(group))

        for domain in domains:
            domain_quota_sum = Mailbox.objects.filter(domain=domain) \
                .values('domain') \
                .annotate(size = Sum('quotasize'))

            if domain_quota_sum:
                domain.quota_sum = domain_quota_sum[0]["size"]

            domain_mailbox_cnt = Mailbox.objects.filter(domain=domain) \
                .values('domain') \
                .annotate(total=Count('domain'))

            if domain_mailbox_cnt:
                domain.mailbox_cnt = domain_mailbox_cnt[0]["total"]

        context['domains'] = domains
        return context
