# global Python imports
import logging

# Django specific imports
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import get_object_or_404
from braces.views import LoginRequiredMixin
from django.db.models import Sum, Count
from django.contrib.auth.models import Group
from django.urls import reverse_lazy

# App specific imports
from core.models import Domain
from core.models import Mailbox
from core.models import Alias
from core.forms import DomainEditForm

# Get an instance of the logger
logger = logging.getLogger(__name__)

class DomainDetailView(LoginRequiredMixin, TemplateView):
    template_name = "domain_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        domain = get_object_or_404(Domain, pk=self.kwargs['pk'])

        domain_quota_sum = Mailbox.objects.filter(domain = domain.id) \
            .values('domain') \
            .annotate(size = Sum('quotasize'))
        if domain_quota_sum:
            domain.quota_sum = domain_quota_sum[0]["size"]

        domain_mailbox_cnt = Mailbox.objects.filter(domain = domain.id) \
            .values('domain') \
            .annotate(total = Count('domain'))
        if domain_mailbox_cnt:
            domain.mailbox_cnt = domain_mailbox_cnt[0]["total"]

        context['domain'] = domain

        mailboxes = Mailbox.objects.filter(domain = self.kwargs['pk'])
        for mailbox in mailboxes:
            if mailbox.user.groups.filter(name="DA_{}".format(domain)):
                mailbox.is_domain_admin = True
        context['mailboxes'] = mailboxes

        aliases = Alias.objects.filter(domain = self.kwargs['pk'])
        context['aliases'] = aliases

        return context

class DomainCreateView(LoginRequiredMixin, CreateView):
    model = Domain
    form_class = DomainEditForm
    template_name = "domain_form.html"
    login_url = "/login/"
    success_url = reverse_lazy("dashboard_view")

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            return HttpResponseRedirect(self.success_url)
        else:
            return super(DomainCreateView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        group = Group.objects.get_or_create(name='DA_{}'.format(form.instance.domain))
        return super(DomainCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(DomainCreateView, self).get_context_data(**kwargs)

        context['title'] = "Domain | Add a domain"
        context['nav_section'] = "domain"
        context['nav_sub_section'] = "domain_create"
        return context
