# global Python imports
import logging

# Django specific imports
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from braces.views import LoginRequiredMixin
from django.contrib.auth.models import Group
from django.core.validators import validate_email
from django.core.exceptions import ValidationError

# App specific imports
from core.models import Mailbox
from core.models import User
from core.models import Domain
from core.forms import MailboxAddForm
from core.forms import MailboxEditForm

# Get an instance of the logger
logger = logging.getLogger(__name__)

class MailboxListView(LoginRequiredMixin, ListView):
    model = Mailbox
    context_object_name = 'mailboxes'
    template_name = "mailbox_list.html"
    # prevent 404 if there is no data yet
    # https://stackoverflow.com/a/37277192/10013881
    allow_empty = True
    # https://stackoverflow.com/a/42698234/10013881
    #ordering = ['name']

    login_url = "/login/"

    def get_queryset(self):
        queryset = Mailbox.objects.filter(domain = self.kwargs['domain'])
        return queryset

    def get_context_data(self, **kwargs):
        context = super(MailboxListView, self).get_context_data(**kwargs)
        context['current_domain'] = Domain.objects.filter(id=self.kwargs['domain']).values_list('domain', flat=True)[0]
        context['title'] = "Mailbox | List of Mailboxes"
        context['nav_section'] = "mailbox"
        context['nav_sub_section'] = "mailbox_list"
        return context

class MailboxCreateView(LoginRequiredMixin, CreateView):
    model = Mailbox
    form_class = MailboxAddForm
    template_name = "mailbox_form.html"
    login_url = "/login/"

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            return HttpResponseRedirect(self.success_url)
        else:
            return super(MailboxCreateView, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MailboxCreateView, self).get_context_data(**kwargs)

        context['title'] = "Mailbox | Add a mailbox"
        context['nav_section'] = "mailbox"
        context['nav_sub_section'] = "mailbox_create"
        return context

    def get_success_url(self):
        domain = self.object.domain
        return reverse('core:domain-detail-view', kwargs={'pk': domain.id})

    def form_valid(self, form):
        # read domain from url parameter
        domain = self.kwargs['domain']
        form.instance.domain = get_object_or_404(Domain, pk=self.kwargs['domain'])

        logger.debug('will create mailbox for domain {}'.format(form.instance.domain))

        try:
            validate_email("{}@{}".format(form.instance.username, form.instance.domain))
        except ValidationError:
            form.add_error('username',
                 "Address {}@{} is not a valid email address." \
                 .format(form.instance.username, form.instance.domain))
            return super(MailboxCreateView, self).form_invalid(form)

        if '-' in form.instance.username:
            form.add_error("username",
                "The username {} contains a hyphen character. " \
                "This mailsystem uses the hyphen character in the user part " \
                "of an email address as the tag for a mailbox.".format(form.instance.username))
            return super(MailboxCreateView, self).form_invalid(form)

        # add a Django user so users can login to Django
        user = User.objects.create_user(
            email = "%s@%s" % (form.instance.username, form.instance.domain),
            password = form.instance.password
        )
        user.save()
        logger.info('user {} created'.format(user.email))

        if form.cleaned_data["is_domain_admin"]:
            logger.info("user will be domain admin DA_{}".format(form.instance.domain))
            da_group = Group.objects.get(name = 'DA_{}'.format(form.instance.domain))
            da_group.user_set.add(user)

        form.instance.user = user

        return super(MailboxCreateView, self).form_valid(form)

class MailboxEditView(LoginRequiredMixin, UpdateView):
    model = Mailbox
    # http://stackoverflow.com/q/39020226/7523861
    form_class = MailboxEditForm
    template_name = "mailbox_form.html"

    login_url = "/login/"

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            return HttpResponseRedirect(self.success_url)
        else:
            return super(MailboxEditView, self).post(request, *args, **kwargs)

    def get_success_url(self):
        domain = self.object.domain
        return reverse('core:domain-detail-view', kwargs={'pk': domain.id})

    def get_context_data(self, **kwargs):
        context = super(MailboxEditView, self).get_context_data(**kwargs)

        context['update_or_create'] = "update"
        return context

class MailboxDeleteView(LoginRequiredMixin, DeleteView):
    model = Mailbox
    template_name = "mailbox_confirm_delete.html"

    login_url = "/login/"

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            return HttpResponseRedirect(self.get_success_url(self))
        else:
            return super(MailboxDeleteView, self).post(request, *args, **kwargs)

    def get_success_url(self):
        domain = self.object.domain
        return reverse('core:domain-detail-view', kwargs={'pk': domain.id})

    def delete(self, request, *args, **kwargs):
        object = self.get_object()

        logger.info("[{}] will delete mailbox {}@{}".format(request.user, object.username, object.domain))
        response = super(MailboxDeleteView, self).delete(request, *args, **kwargs)

        logger.info("[{}] will delete Django user {}@{}".format(request.user, object.username, object.domain))
        User.objects.get(email = '{}@{}'.format(object.username, object.domain)).delete()

        return response
