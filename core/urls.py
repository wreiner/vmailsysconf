from django.conf.urls import url
from django.urls import path, re_path

# Domain
from .views.domain import DomainCreateView
from .views.domain import DomainDetailView

from .views.mailbox import MailboxListView
from .views.mailbox import MailboxCreateView
from .views.mailbox import MailboxEditView
from .views.mailbox import MailboxDeleteView

from .views.alias import AliasCreateView
from .views.alias import AliasEditView
from .views.alias import AliasDeleteView

app_name = "core"

urlpatterns = [
    # domain stuff
    re_path(r'^create/$',                   DomainCreateView.as_view(),     name='domain-create-view'),
    re_path(r'^detail/(?P<pk>[0-9]+)/$',    DomainDetailView.as_view(),     name='domain-detail-view'),

    # mailbox stuff
    re_path(r'^mailbox/list/(?P<domain>[0-9]+)/$',         MailboxListView.as_view(),    name='mailbox-list-view'),
    re_path(r'^mailbox/create/$',       MailboxCreateView.as_view(),    name='mailbox-create-view'),
    re_path(r'^mailbox/create/(?P<domain>[0-9]+)/$',       MailboxCreateView.as_view(),    name='mailbox-create-view-of-domain'),
    re_path(r'^mailbox/edit/(?P<pk>[0-9]+)/$',          MailboxEditView.as_view(),    name='mailbox-edit-view'),
    re_path(r'^mailbox/delete/(?P<pk>[0-9]+)/$',        MailboxDeleteView.as_view(),    name='mailbox-delete-view'),

    # alias stuff
    re_path(r'^alias/create/(?P<domain>[0-9]+)/$',                  AliasCreateView.as_view(),  name='alias-create-view-of-domain'),
    re_path(r'^alias/edit/(?P<domain>[0-9]+)/(?P<pk>[0-9]+)/$',     AliasEditView.as_view(),    name='alias-edit-view'),
    re_path(r'^alias/delete/(?P<pk>[0-9]+)/$',                      AliasDeleteView.as_view(),  name='alias-delete-view'),
]
