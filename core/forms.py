from django import forms
from django.forms import ModelForm, Textarea, CheckboxInput

from .models import *

class DomainEditForm(ModelForm):
    class Meta:
        model = Domain
        exclude = ('user',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

class MailboxAddForm(ModelForm):
    is_domain_admin = forms.BooleanField(required = False, label = 'User is domain admin')
    class Meta:
        model = Mailbox
        exclude = ('user', 'quotastring', 'domain')
        widgets = {
            #'password': forms.PasswordInput()
            #'is_domain_admin' : CheckboxInput(attrs={'class': 'required checkbox form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

class MailboxEditForm(ModelForm):
    class Meta:
        model = Mailbox
        exclude = ('user', 'quotastring', 'domain', 'password', 'username')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

class AliasEditForm(ModelForm):
    class Meta:
        model = Alias
        exclude = ('domain',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
