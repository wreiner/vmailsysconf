# global Python imports
import logging

# Django specific imports
from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from braces.views import LoginRequiredMixin

# App specific imports
from .models import Domain
from .forms import DomainEditForm

# Get an instance of the logger
logger = logging.getLogger(__name__)

class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard.html"

    def get_context_data(sef, **kwargs):
        print("in get context")
        context = super().get_context_data(**kwargs)
        context['domains'] = Domain.objects.all()
        print(context['domains'])
        return context

class DomainCreateView(LoginRequiredMixin, CreateView):
    model = Domain
    form_class = DomainEditForm
    template_name = "domain_form.html"
    login_url = "/login/"

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            return HttpResponseRedirect(self.success_url)
        else:
            return super(DomainCreateView, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DomainCreateView, self).get_context_data(**kwargs)

        context['title'] = "Domain | Add a domain"
        context['nav_section'] = "domain"
        context['nav_sub_section'] = "domain_create"
        return context
