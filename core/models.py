from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from .functions import SHA512Crypt

# https://www.fomfus.com/articles/how-to-use-email-as-username-for-django-authentication-removing-the-username
class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

class User(AbstractUser):
    """User model."""

    username = None
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

class Domain(models.Model):
    """
    Holds domain config.
    """

    admins = models.ManyToManyField(User)
    domain = models.CharField(max_length=64, unique=True)
    enabled = models.BooleanField(default=False)
    # whole domain has 5GB default quota size
    mailbox_size_quota = models.IntegerField(default=5)
    # the domain can use the mailbox_size_quota for mailbox_count_quota mailboxes
    mailbox_count_quota = models.IntegerField(default=5)

    def __str__(self):
        return(self.domain)

class Mailbox(models.Model):
    """
    Holds the actual mailbox config.
    """

    user = models.ForeignKey(User, on_delete=models.PROTECT)
    name = models.CharField(max_length=100)
    username = models.CharField(max_length=64)
    domain = models.ForeignKey(Domain, on_delete=models.PROTECT)
    password = models.CharField(max_length=255)
    quotastring = models.CharField(max_length=255, default='maildir:storage=1G:messages=+0')
    quotasize = models.IntegerField(default=1)
    enabled = models.BooleanField(default=False)
    sendonly = models.BooleanField(default=False)
    memo_txt = models.CharField(max_length=100, blank=True)

    class Meta:
        unique_together = ('username', 'domain')

    def save(self, *args, **kwargs):
        # https://mad9scientist.com/dovecot-password-creation-php/
        sha512c = SHA512Crypt()

        # do not store plaintext passwords in db
        self.password = sha512c.get_crypt(self.password)

        # build quota string
        self.quotastring = 'maildir:storage={}G:messages=+0'.format(self.quotasize)

        super().save(*args, **kwargs)  # Call the "real" save() method.

    def __str__(self):
        return("%s@%s" % (self.username, self.domain))

class Alias(models.Model):
    source = models.CharField(max_length=128)
    destination = models.CharField(max_length=128)
    enabled = models.BooleanField(default=False)
    domain = models.ForeignKey(Domain, on_delete=models.PROTECT)

    def __str__(self):
        return("%s - %s" % (self.source, self.destination))
